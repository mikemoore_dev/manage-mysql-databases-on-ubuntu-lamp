# Create a database by running command 'sudo bash create_db.sh'
# Remove a database by running command 'sudo bash remove_db.sh'
# Backup a database by running command 'sudo bash backup_db.sh'
#
# Parameters:
#
#	Either pass the database name as a single parameter or enter it when prompted
# 
###
#
# Backup all databases by running command 'sudo bash backup_all.sh'
#
# Parameters:
#
#	Either pass the MySQL root password as a single parameter or enter it when prompted
#
#
# Follow the prompts in all cases