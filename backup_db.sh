#!/bin/bash

# Create a new MySQL database

echo "";
if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

# Request the name of the database if it has not been passed as a parameter
if [ $# -eq 0 ]
	then 
	echo -e "Please enter a name for your new database: "
	read DB_NAME;
else
	DB_NAME=$1;
fi

echo "";
read -p "You are about to backup ${DB_NAME}, is this correct? <y/N> " prompt
if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
then

	echo "";
	echo -e "Enter the password for the root MySQL user: "
	read -s ROOT_PWORD;

	#Check if database exists
	RESULT=`mysqlshow -u root -p${ROOT_PWORD} ${DB_NAME}| grep -v Wildcard | grep -o ${DB_NAME}`
	if [ "${RESULT}" == "${DB_NAME}" ]; then

		echo "";
		echo "Taking a backup of ${DB_NAME}.";
		echo "";

		BACKUP_FOLDER="./backups"
		if [ -d "$BACKUP_FOLDER" ]; then
			echo "";
		else
			mkdir ${BACKUP_FOLDER};
		fi

		DB_BACKUP_FOLDER="${BACKUP_FOLDER}/${DB_NAME}"
		if [ -d "$DB_BACKUP_FOLDER" ]; then
			echo "";
		else
			mkdir ${DB_BACKUP_FOLDER};
		fi

		# Backup the database
		DATE=`date +%Y-%m-%d:%H:%M:%S`
		DB_BACKUP_FILE="${DB_BACKUP_FOLDER}/${DB_NAME}_${DATE}.sql";
		mysqldump -u root -p${ROOT_PWORD} ${DB_NAME} > ${DB_BACKUP_FILE};

		echo "";
		echo "${DB_NAME} has been exported to ${DB_BACKUP_FILE}. Have a great day!";
		echo "";

	else

		echo "";
		echo "Database ${DB_NAME} doesn't exist. Try again using a different name.";
		echo "";
		exit;

	fi

else
	echo "";
	echo "You have chosen NOT to backup ${DB_NAME}.";
	echo "";
	exit;
fi