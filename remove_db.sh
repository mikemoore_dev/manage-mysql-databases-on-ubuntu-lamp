#!/bin/bash

# Remove an old MySQL database

echo "";
if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

# Request the name of the database if it has not been passed as a parameter
if [ $# -eq 0 ]
	then 
	echo -e "Enter the name of the database you would like to delete: "
	read DB_NAME;
else
	DB_NAME=$1;
fi

echo "";
echo -e "Enter the password for the root MySQL user: "
read -s ROOT_PWORD;

# Request confirmation before deletion
	echo "";
	read -p "You are about to completely remove ${DB_NAME}, are you sure? <y/N> " prompt
	if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
	then

	#Check if database exists
	RESULT=`mysqlshow -u root -p${ROOT_PWORD} ${DB_NAME}| grep -v Wildcard | grep -o ${DB_NAME}`
	if [ "${RESULT}" == "${DB_NAME}" ]; then
	    
	    # Remove the database
		TMP="./tmp"
		SQL_TMP="${TMP}/tmp.sql"
		SQL="DROP DATABASE ${DB_NAME};"
		mkdir ${TMP};
		touch ${SQL_TMP};
		echo -e ${SQL} >> ${SQL_TMP}

		mysql -u root -p${ROOT_PWORD} < ${SQL_TMP};
		rm -rf ${TMP}

		echo "";
		echo "${DB_NAME} has been removed. Have a great day!";
		echo "";

	else
		echo "";
		echo "${DB_NAME} does not exist, please try again";
		echo "";
		exit;
	fi

else
	echo "";
	echo "${DB_NAME} has NOT been removed.";
	echo "";
	exit;
fi