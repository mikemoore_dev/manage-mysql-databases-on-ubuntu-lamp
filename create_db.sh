#!/bin/bash

# Create a new MySQL database

echo "";
if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

# Request the name of the database if it has not been passed as a parameter
if [ $# -eq 0 ]
	then 
	echo -e "Please enter a name for your new database: "
	read DB_NAME;
else
	DB_NAME=$1;
fi

if [[ $DB_NAME != *"_local"* ]]
then
	echo "";
	read -p "It is recommended that your database name includes the suffix '_local', would you like to append this before continuing? <y/N> " prompt
	if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
	then
		DB_NAME="${DB_NAME}_local";
	fi
fi

echo "";
read -p "You are about to create ${DB_NAME}, is this correct? <y/N> " prompt
if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
then

	echo "";
	echo -e "Enter the password for the root MySQL user: "
	read -s ROOT_PWORD;

	#Check if database exists
	RESULT=`mysqlshow -u root -p${ROOT_PWORD} ${DB_NAME}| grep -v Wildcard | grep -o ${DB_NAME}`
	if [ "${RESULT}" == "${DB_NAME}" ]; then

		echo "";
		echo "Database ${DB_NAME} already exists. Try again using a different name.";
		echo "";
		exit;

	else

		# Create the database
		TMP="./tmp"
		SQL_TMP="${TMP}/tmp.sql"
		SQL="CREATE DATABASE IF NOT EXISTS ${DB_NAME};"
		mkdir ${TMP};
		touch ${SQL_TMP};
		echo -e ${SQL} >> ${SQL_TMP}

		mysql -u root -p${ROOT_PWORD} < ${SQL_TMP};
		rm -rf ${TMP}

		echo "";
		echo "MySQL database ${DB_NAME} has been created. Have a great day!";
		echo "";

	fi

else
	echo "";
	echo "You have chosen NOT to create ${DB_NAME}.";
	echo "";
	exit;
fi