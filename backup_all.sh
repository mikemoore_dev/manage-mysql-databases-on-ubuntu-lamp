#!/bin/bash

# Backup all MySQL databases

echo "";
if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

# Request the root db password if it has not been passed as a parameter
if [ $# -eq 0 ]
	then
	echo ""; 
	echo -e "Enter the password for the root MySQL user: "
	read -s ROOT_PWORD;
else
	ROOT_PWORD=$1;
fi

BACKUP_FOLDER="./backups"
if [ -d "$BACKUP_FOLDER" ]; then
	echo "";
else
	mkdir ${BACKUP_FOLDER};
fi

mysql -s -r -uroot -p${ROOT_PWORD} -e 'show databases' -N | while read DB_NAME; do 
	
	DB_BACKUP_FOLDER="${BACKUP_FOLDER}/${DB_NAME}"
	if [ -d "$DB_BACKUP_FOLDER" ]; then
		echo "";
	else
		mkdir ${DB_BACKUP_FOLDER};
	fi

	DATE=`date +%Y-%m-%d:%H:%M:%S`

	DB_BACKUP_FILE="${DB_BACKUP_FOLDER}/${DB_NAME}_${DATE}.sql";

    mysqldump -uroot -p${ROOT_PWORD} --complete-insert --single-transaction "${DB_NAME}" > "${DB_BACKUP_FILE}"; 
done

exit;